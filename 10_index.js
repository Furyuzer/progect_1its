var fs = require('fs');

 //Подключение к локальному серверу                                    

var http = require('http');
var server = http.createServer(function(req, res){                  //Создаем сервер
    console.log("URL страницы: " + req.url)                         //Выводим отслеживаемый url
    res.writeHead(200,{'Content-Type':'text/html;charset=utf-8'});
    var myReadShort = fs.createReadStream(__dirname + '/10_index.html','utf8');//Выводим html 
    myReadShort.pipe(res);                                                                     
});

server.listen(3000,'127.0.0.1');                                    //Отслеживемый адрес и порт
console.log("ПОРТ 3000")