var express= require('express');

var app = express(); //Вызываем функционал express

app.get('/',function(req, res) {
    res.send('Hello');
});
app.get('/news',function(req, res) { 
    res.send('News');
});
app.get('/news/:name/:id',function(req, res) {
    res.send('ID is - ' + req.params.name + req.params.id);
});
app.listen(3000); //Порт

