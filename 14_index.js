var express= require('express');

//Подключаем парсер
var bodyParser = require('body-parser')

//Вызываем функционал express
var app = express(); 
//Получаем данные из Post-запрроса
var urlencodedParser = bodyParser.urlencoded({ extended: false })

//Определяем движок 
app.set('view engine','ejs'); 

//Определяем директорию с контентом
app.use('/public', express.static('public')); 
//Главная
app.get('/',function(req, res) {  
    res.render('index');
});
app.get('/index',function(req, res) { 
    res.render('index');
});

//О нас
app.get('/about',function(req, res) {
    res.render('about');
});

app.post('/about', urlencodedParser, function(req, res) {
    if (!req.body) return res.sendStatus(400)
    console.log(req.body) //выводим данные в консоль
    res.render('post-about',{data: req.body});
});

 //Новости
app.get('/news/:id',function(req, res) {
    res.render('news',{newsId: req.params.id});
});
//Порт
app.listen(3000); 

