console.log("Hello world");
console.log(__dirname);     //выводит располоение файла
console.log(__filename);    //выводит расположение файла и имя

var x = 0;
if (x == 0)                 //условие,при x = 0, выведет "work"
  console.log("work");
for (var i = 0;i < 5;i++) //цикл,на кождой итерации выводящий i
  console.log(i);

setTimeout(function() {     //С двухсекундной задеркой выведет
    console.log("Привет!")  //"Привет!"
}, 2000);  