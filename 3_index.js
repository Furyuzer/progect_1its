var counter = require('./3_array'); //Подключаем файл
console.log(counter([1,2,3,4,5]))


function test() {
  console.log("Привет!") //Обыкновенна функция,при обращении                        //
}                        //выводит "Привет!"
test();

function call(f_name){ //В качестве аргумента принимает функцию для вызова
    f_name();
}

var outputSomething = function() { //Именованная функция
  console.log("ТЕСТ")
};

call(outputSomething);

