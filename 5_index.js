var events = require('events');

var myEmit = new events.EventEmitter();

myEmit.on('some_event', function(text){
    console.log(text);
}); //Отслеживаем событие,при завершении срабатывает функция

myEmit.emit('some_event','Событие сработало');//Вывзываем событие

var util = require('util');

var Cars = function(model){ //Конструктор объектов
    this.model = model;
};
util.inherits(Cars, events.EventEmitter)

var bmv = new Cars("BMW")