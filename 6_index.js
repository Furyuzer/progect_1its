var fs = require('fs');

var file_readed = fs.readFileSync('6_text.txt','utf8');//Читаем текст из файла
console.log(file_readed);

var message = "Hello world!\n" + file_readed;
fs.writeFileSync('6_some_new_file.txt',message);//Создаёт новый файл, записывает в него информацию

fs.readFile('6_text.txt','utf8',function(err,data){//Асинхронное чтение файла
    console.log(data);
});