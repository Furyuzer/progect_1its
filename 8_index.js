var http = require('http');
var server = http.createServer(function(req, res){                  //Создаем сервер
    console.log("URL страницы: " + req.url)                         //Выводим отслеживаемый url
    res.writeHead(200,{'Content-Type':'text/plain;charset=utf-8'}); //Настраиваем контент
    res.end('Привет мир!');                                         //Выводим текст                            
});

server.listen(3000,'127.0.0.1');                                    //Отслеживемый адрес и порт
console.log("ПОРТ 3000")